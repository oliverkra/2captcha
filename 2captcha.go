package twocaptcha

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"time"
)

const (
	ErrNotReady = "CAPCHA_NOT_READY"
	ErrTimeout  = "CAPCHA_TIMEOUT"
)

var (
	baseURL = &url.URL{Host: "2captcha.com", Scheme: "https", Path: "/"}

	Debug = false
)

// Client .
type Client struct {
	apiKey string
}

// New .
func New(apikey string) Client {
	return Client{apiKey: apikey}
}

type responseAPI struct {
	Status  int64  `json:"status"`
	Request string `json:"request"`
}

func (c *Client) request(path string, params map[string]string) (string, error) {
	url := baseURL.ResolveReference(&url.URL{Path: path})
	q := url.Query()
	q.Add("key", c.apiKey)
	q.Add("json", "1")
	for k, v := range params {
		q.Add(k, v)
	}
	url.RawQuery = q.Encode()

	resp, err := http.Get(url.String())
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	content, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	if Debug {
		log.Println("2CAPTCHA RESPONSE", string(content))
	}

	body := &responseAPI{}
	if err := json.Unmarshal(content, &body); err != nil {
		return "", err
	}

	if body.Status != 1 {
		return "", errors.New(body.Request)
	}
	return body.Request, nil
}

// createTaskRecaptcha create the task to process the recaptcha, returns the taskID
func (c *Client) createTaskRecaptcha(websiteURL string, recaptchaKey string) (string, error) {
	if Debug {
		log.Printf("2CAPTCHA REQUEST: %s > %s", websiteURL, recaptchaKey)
	}
	return c.request("/in.php", map[string]string{
		"method":    "userrecaptcha",
		"googlekey": recaptchaKey,
		"pageurl":   websiteURL,
	})
}

// getTaskResult returns the response of the specific taskID
func (c *Client) getTaskResult(taskID string) (string, error) {
	return c.request("/res.php", map[string]string{
		"action": "get",
		"id":     taskID,
	})
}

func (c *Client) SendRecaptchaWithTimeAndRetries(websiteURL string, recaptchaKey string, retries int, sleep time.Duration) (string, error) {
	taskID, err := c.createTaskRecaptcha(websiteURL, recaptchaKey)
	if err != nil {
		return "", err
	}

	time.Sleep(sleep)
	for i := 1; i <= retries; i++ {
		resp, err := c.getTaskResult(taskID)
		if err == nil {
			return resp, nil
		}

		if err.Error() != ErrNotReady {
			return "", err
		}
		time.Sleep(sleep)
	}

	return "", errors.New(ErrTimeout)
}

// SendRecaptcha Method to encapsulate the processing of the recaptcha
// Given a url and a key, it sends to the api and waits until
// the processing is complete to return the evaluated key
func (c *Client) SendRecaptcha(websiteURL string, recaptchaKey string) (string, error) {
	return c.SendRecaptchaWithTimeAndRetries(websiteURL, recaptchaKey, 5, time.Second*5)
}
